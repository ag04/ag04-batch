#!/usr/bin/env groovy

pipeline {
    agent any

    options {
        buildDiscarder(logRotator(numToKeepStr: '5', artifactNumToKeepStr: '5'))
        skipDefaultCheckout()
        ansiColor('xterm')
        timestamps()
    }

    tools {
        jdk '1.8.0_111'
        maven '3.3.9'
    }

    stages {
        stage('Clean workspace') {
            steps {
                sh 'ls -lah'
                deleteDir()
                sh 'ls -lah'
            }
        }

        stage('Checkout source') {
            steps {
                checkout scm
            }
        }

        stage('Build maven') {
            steps {
                timeout(time: 30, unit: 'MINUTES') {
                    sh 'mvn clean package --debug'
                }
            }

            post {
                always {
                    junit '**/target/surefire-reports/*.xml'
                }

                success {
                    archiveArtifacts artifacts: '**/target/*.jar', fingerprint: true
                }
            }
        }

    }

    post {
        failure {
            script {
                if (isReleaseBranchBuild()) {
                    mattermostSend color: 'danger', message: "**Build failed** :thumbsdown:\n[${currentBuild.displayName}](${currentBuild.absoluteUrl})\n**Job Name**\n[${env.JOB_NAME}](${env.JOB_URL})\n**Branch Name**\n${env.BRANCH_NAME}"
                }
            }
        }

        success {
            script {
                if (isReleaseBranchBuild()) {
                    mattermostSend color: 'good', message: "**Build succeded** :thumbsup:\n[${currentBuild.displayName}](${currentBuild.absoluteUrl})\n**Job Name**\n[${env.JOB_NAME}](${env.JOB_URL})\n**Branch Name**\n${env.BRANCH_NAME}"
                }
            }
        }

        unstable {
            script {
                if (isReleaseBranchBuild()) {
                    mattermostSend color: 'warning', message: "**Build unstable** :disappointed:\n[${currentBuild.displayName}](${currentBuild.absoluteUrl})\n**Job Name**\n[${env.JOB_NAME}](${env.JOB_URL})\n**Branch Name**\n${env.BRANCH_NAME}"
                }
            }
        }
    }
}

def isReleaseBranchBuild() {
    env.BRANCH_NAME ==~ /^((master)|(develop)|(release-\d\.\d))$/
}

def isPullRequestBuild() {
    if (env.CHANGE_ID) {
        return true
    }

    return false
}

// TODO: remove this after "cloudbees-bitbucket-branch-source" plugin is updated
def extractRepositoryDetails() {
    def matcher = (env.CHANGE_URL =~ /.*\/(.+)\/(.+)\/pull-requests\/\d+/)

    if (matcher.matches()) {
        return [owner: matcher.group(1), name: matcher.group(2)]
    }

    throw new IllegalStateException('Owner and repository should always be extractable from URL')
}