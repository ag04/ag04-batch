package com.ag04.batch.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Represents single type of batch job.
 * 
 * @author dmadunic
 *
 */
@Entity
@Table(name = "BATCH_JOB")
@SequenceGenerator(name="BatchJobSeq", sequenceName="BATCH_JOB_SEQ")
public class BatchJob {

	@Id
    @Column(name = "JOB_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BatchJobSeq")
	private Long id;
	
	@Column(name = "JOB_NAME", nullable = false, length=40)
	private String name;
	
	@Column(name = "JOB_DESC", nullable = true, length=255)
	private String description;
	
	@Column(name = "JOB_ONCE_PER_DAY")
	private boolean oncePerDayJob = false;
	
	@Column(name = "JOB_ENABLED")
	private boolean enabled = true;
	
	//--- set / get methods ---------------------------------------------------
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isOncePerDayJob() {
		return oncePerDayJob;
	}
	public void setOncePerDayJob(boolean oncePerDayJob) {
		this.oncePerDayJob = oncePerDayJob;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	
}
