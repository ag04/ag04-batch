package com.ag04.batch.model;


public enum ExecutionStatus {
	SUCCESS("SUCCESS", 1L), FAIL("FAIL", 2L), IN_PROGRESS("IN_PROGRESS", 3L);

	public String name;
	public Long value;

	ExecutionStatus(final String p_name, final Long p_value) {
		name = p_name;
		value = p_value;
	}

	//--- util methods --------------------------------------

	/**
	 * @param p_enumValue - enum value
	 * @return An {@link ExecutionStatus} where {@link ExecutionStatus}
	 *         {@link #getValue()} equals enumValue.
	 */
	public static ExecutionStatus getEnumByValue(final Object p_enumValue) {
		for (int i = 0; i < ExecutionStatus.values().length; ++i) {
			ExecutionStatus result = ExecutionStatus.values()[i];
			if (result.getValue().equals(p_enumValue)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * @param p_enumName - name of the enum
	 * @return An {@link ExecutionStatus} where {@link ExecutionStatus}
	 *         {@link #getName()} equals enumName.
	 */
	public static ExecutionStatus getEnumByName(final String p_enumName) {
		for (int i = 0; i < ExecutionStatus.values().length; ++i) {
			ExecutionStatus result = ExecutionStatus.values()[i];
			if (result.getName().equals(p_enumName)) {
				return result;
			}
		}
		return null;
	}

	// --- set / get methods --------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}
}
