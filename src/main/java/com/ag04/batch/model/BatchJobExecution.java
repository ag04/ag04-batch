package com.ag04.batch.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

/**
 * Record of execution of single instance of specific job.
 * 
 * @author dmadunic
 *
 */
@Entity
@Table(name = "BATCH_JOB_EXEC")
@SequenceGenerator(name="BatchJobExecSeq", sequenceName="BATCH_JOB_EXEC_SEQ")
public class BatchJobExecution {
	public static final int MAX_EXIT_MSG_LENGHT = 2048;

	public BatchJobExecution() {
		//
		runDate = new LocalDate();
	}
	
	public BatchJobExecution(BatchJob p_job) {
		job = p_job;
		runDate = new LocalDate();
	}
	
	public BatchJobExecution(BatchJob p_job, Map<String, String> p_jobParams) {
		job = p_job;
		runDate = new LocalDate();
		jobParams = p_jobParams;
	}
	
	@Id
    @Column(name = "JOB_EXEC_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BatchJobExecSeq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "JOB_ID")
	private BatchJob job;
	
	@Column(name = "JOB_EXEC_RUN_DATE", nullable = true)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate runDate;
	
	@Column(name = "JOB_EXEC_START_TIME", nullable = true)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime jobStartTime;
	
	@Column(name = "JOB_EXEC_END_TIME", nullable = true)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime jobEndTime;
	
	@Transient
	private ExecutionStatus status;
	
	@Column(name = "JOB_EXEC_STATUS_ID", nullable = false)
	private Long statusId;
	
	@Column(name = "JOB_EXEC_EXIT_MSG", length=2048, nullable = true)
	private String exitMessage;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@JoinTable(name="BATCH_JOB_EXEC_PARAMS", joinColumns=@JoinColumn(name="JOB_EXEC_ID"))
	@MapKeyColumn (name="JOB_PARAM_KEY")
	@Column(name="JOB_PARAM_VALUE")
	private Map<String, String> jobParams = new HashMap<String, String>();
	
	//--- set/get methods -----------------------------------------------------
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BatchJob getJob() {
		return job;
	}

	public void setJob(BatchJob job) {
		this.job = job;
	}

	public LocalDate getRunDate() {
		return runDate;
	}

	public void setRunDate(LocalDate runDate) {
		this.runDate = runDate;
	}

	public String getExitMessage() {
		return exitMessage;
	}

	public void setExitMessage(final String exitMessage) {
		if (exitMessage != null && exitMessage.length() > MAX_EXIT_MSG_LENGHT) {
			this.exitMessage = exitMessage.substring(0, MAX_EXIT_MSG_LENGHT - 4);
			this.exitMessage = this.exitMessage + "...";
		} else {
			this.exitMessage = exitMessage;	
		}
		
	}
	
	//---- special enum set / get methods --------------------
	
	public ExecutionStatus getStatus() {
		if (status == null) {
			status = ExecutionStatus.getEnumByValue(statusId);
		}
		return status;
	}

	public void setStatus(ExecutionStatus status) {
		this.status = status;
		statusId = status.getValue();
	}
	
//	public List<BatchJobExecutionParams> getJobParams() {
//		return jobParams;
//	}
//
//	public void setJobParams(List<BatchJobExecutionParams> jobParams) {
//		this.jobParams = jobParams;
//	}

	public Map<String, String> getJobParams() {
		return jobParams;
	}

	public void setJobParams(Map<String, String> jobParams) {
		this.jobParams = jobParams;
	}

	public DateTime getJobStartTime() {
		return jobStartTime;
	}

	public void setJobStartTime(DateTime jobStartTime) {
		this.jobStartTime = jobStartTime;
	}

	public DateTime getJobEndTime() {
		return jobEndTime;
	}

	public void setJobEndTime(DateTime jobEndTime) {
		this.jobEndTime = jobEndTime;
	}

}
