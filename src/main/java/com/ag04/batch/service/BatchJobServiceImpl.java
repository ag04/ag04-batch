package com.ag04.batch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ag04.batch.dao.repository.BatchJobRepository;
import com.ag04.batch.model.BatchJob;
/**
 * 
 * @author dmadunic
 * 
 * @since 21.01.2014
 */
@Service
public class BatchJobServiceImpl implements BatchJobService {

	@Autowired
	public transient BatchJobRepository jobRepository;
	
	public BatchJob findByName(String p_name) {
		return jobRepository.findByName(p_name);
	}

	public BatchJob findById(Long p_id) {
		return jobRepository.findOne(p_id);
	}

	public BatchJob save(BatchJob p_job) {
		return jobRepository.saveAndFlush(p_job);
	}
	
	public List<BatchJob> findAll() {
		return jobRepository.findAll();
	}
	
	//--- set / get methods --------------------------------------------------

	public BatchJobRepository getJobRepository() {
		return jobRepository;
	}

	public void setJobRepository(BatchJobRepository jobRepository) {
		this.jobRepository = jobRepository;
	}

}
