package com.ag04.batch.service;

import java.util.List;

import com.ag04.batch.model.BatchJob;

/**
 * 
 * @author dmadunic
 * @since 21.01.2014
 */
public interface BatchJobService {
	
	BatchJob findByName(String p_name);
	
	BatchJob findById(Long p_id);
	
	BatchJob save(BatchJob p_job);
	
	List<BatchJob> findAll();
}
