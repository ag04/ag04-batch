package com.ag04.batch.service;

import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ag04.batch.model.BatchJob;
import com.ag04.batch.model.BatchJobExecution;
import com.ag04.batch.model.ExecutionStatus;
/**
 * 
 * @author dmadunic
 * @since 21.01.2014
 * 
 */
public interface BatchJobExecutionService {

	BatchJobExecution findById(Long p_id);
	
	List<BatchJobExecution> findByJobAndStatus(BatchJob p_job, ExecutionStatus p_status, Pageable p_pageable);
	
	List<BatchJobExecution> findExecutionsForJob(BatchJob p_job, Pageable p_pageable);
	
	List<BatchJobExecution> findJobExecutionOnDateWithParamOfValue(BatchJob p_job, LocalDate p_runDate, String p_paramName, String p_paramValue);
	
	List<BatchJobExecution> findJobExecutionsOnDateOfStatus(BatchJob p_job, LocalDate p_runDate, ExecutionStatus p_status, Pageable p_pageable);
	
	BatchJobExecution startNewExecution(BatchJob p_job, Map<String, String> p_jobParams);
	
	BatchJobExecution save(BatchJobExecution p_jobExecution);
	
	int deleteJobExecutionsBeforeDate(LocalDate p_runDateBefore);
	
	Page<BatchJobExecution> findByJobId(Long p_jobId, Pageable p_pageable);
}
