package com.ag04.batch.service;

import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.ag04.batch.dao.repository.BatchJobExecutionRepository;
import com.ag04.batch.model.BatchJob;
import com.ag04.batch.model.BatchJobExecution;
import com.ag04.batch.model.ExecutionStatus;
/**
 * 
 * @author dmadunic
 * @since 21.01.2014
 * 
 */
public class BatchJobExecutionServiceImpl implements BatchJobExecutionService {

	@Autowired
	public transient BatchJobExecutionRepository jobExecutionRepository;
	
	public BatchJobExecution findById(Long p_id) {
		return jobExecutionRepository.findOne(p_id);
	}

	public List<BatchJobExecution> findExecutionsForJob(BatchJob p_job, Pageable p_pageable) {
		return jobExecutionRepository.findByJobId(p_job.getId(), p_pageable);
	}

	public List<BatchJobExecution> findJobExecutionOnDateWithParamOfValue(BatchJob p_job, LocalDate p_runDate, String p_paramName,
			String p_paramValue) {
		return jobExecutionRepository.findJobExecutionOnDateWithParamOfValue(p_job.getId(), p_runDate, p_paramName, p_paramValue);
	}

	public BatchJobExecution save(BatchJobExecution p_jobExecution) {
		return jobExecutionRepository.saveAndFlush(p_jobExecution);
	}

	public List<BatchJobExecution> findByJobAndStatus(BatchJob p_job, ExecutionStatus p_status, Pageable p_pageable) {
		return jobExecutionRepository.findByJobIdAndStatusId(p_job.getId(), p_status.getValue(), p_pageable);
	}
	
	public List<BatchJobExecution> findJobExecutionsOnDateOfStatus(BatchJob p_job, LocalDate p_runDate, ExecutionStatus p_status, Pageable p_pageable) {
		List<BatchJobExecution> executions = null;
		if (p_status == null) {
			executions = jobExecutionRepository.findByJobIdAndRunDate(p_job.getId(), p_runDate, p_pageable);
		} else {
			executions = jobExecutionRepository.findByJobIdAndStatusIdAndRunDate(p_job.getId(), p_status.getValue(), p_runDate, p_pageable);
		}
		return executions;
	}

	public BatchJobExecution startNewExecution(BatchJob p_job, Map<String, String> p_jobParams) {
		BatchJobExecution jobExecution = new BatchJobExecution(p_job, p_jobParams);
		jobExecution.setStatus(ExecutionStatus.IN_PROGRESS);
		jobExecution.setJobStartTime(new DateTime());
		jobExecution = jobExecutionRepository.save(jobExecution);
		return jobExecution;
	}

	@Transactional
	public int deleteJobExecutionsBeforeDate(LocalDate p_runDateBefore) {
		return jobExecutionRepository.deleteJobExecutionsBeforeDate(p_runDateBefore);
	}

	public Page<BatchJobExecution> findByJobId(Long p_jobId, Pageable p_pageable) {
		return jobExecutionRepository.findBatchJobByJobId(p_jobId, p_pageable);
	}
}
