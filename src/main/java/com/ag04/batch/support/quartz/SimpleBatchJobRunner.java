package com.ag04.batch.support.quartz;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
/**
 * 
 * @author dmadunic
 *
 */
public abstract class SimpleBatchJobRunner extends QuartzJobBean {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleBatchJobRunner.class);
    
    protected String jobName;

    @Override
    protected void executeInternal(JobExecutionContext p_executionContext) throws JobExecutionException {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        LOG.info("***** Job {} ---> INVOKED", jobName);
        // check if we can run job ...
        try {
            List<JobExecutionContext> jobs = p_executionContext.getScheduler().getCurrentlyExecutingJobs();
            if (jobs != null && !jobs.isEmpty()) {
                for (JobExecutionContext job : jobs) {
                    // TODO: check this -> maybe we should check for BatchJob instead of this?
                    if (job.getTrigger().equals(p_executionContext.getTrigger()) && !job.getJobInstance().equals(this)) {
                        LOG.info("There's another instance running ---> SKIPPING, job=" + this);
                        return;
                    }
                }
            }
        } catch (SchedulerException ex) {
            LOG.error("***** SchedulerException occurred ---> EXITING!", ex);
            return;
        }

        // execute job ...
        LOG.info("Job {} is valid for execution ----> STARTING", jobName);
        try {
            executeJob();
        } catch (Exception ex) {
            LOG.error("Exception while executing job!", ex);
        }
        LOG.info("***** Job {} ---> FINISHED!", jobName);
    }

    public abstract void executeJob();

    // --- set / get methods --------------------------------------------------

    public String getJobName() {
        return jobName;
    }

    public void setJobName(final String jobName) {
        this.jobName = jobName;
    }

}
