package com.ag04.batch.support.quartz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.ag04.batch.model.BatchJob;
import com.ag04.batch.model.BatchJobExecution;
import com.ag04.batch.model.ExecutionStatus;
import com.ag04.batch.service.BatchJobExecutionService;
import com.ag04.batch.service.BatchJobService;

/**
 * Base class to be extended by other quartz jobs.
 * 
 * @author dmadunic
 * @since 21.01.2014
 */
public abstract class BatchJobRunner extends QuartzJobBean {
	private static final Logger LOG = LoggerFactory.getLogger(BatchJobRunner.class);

	protected String jobName;

	@Autowired
	protected BatchJobExecutionService jobExecutionService;

	@Autowired
	protected BatchJobService jobService;

	protected BatchJob batchJob;

	@Override
	protected void executeInternal(JobExecutionContext p_executionContext) throws JobExecutionException {
    	SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		LOG.debug("***** Job {} ---> INVOKED", jobName);
		// check if we can run job ...
		try {
			List<JobExecutionContext> jobs = p_executionContext.getScheduler().getCurrentlyExecutingJobs();
			if (jobs != null && !jobs.isEmpty()) {
				for (JobExecutionContext job : jobs) {
					// TODO: check this -> maybe we should check for BatchJob
					// instead of this?
					if (job.getTrigger().equals(p_executionContext.getTrigger()) && !job.getJobInstance().equals(this)) {
						LOG.info("There's another instance running ---> SKIPPING, job=" + this);
						return;
					}
				}
			}
		} catch (SchedulerException e) {
			LOG.error("***** SchedulerException occurred ---> EXITING! ", e);
			return;
		}
		// check if job has already been executed today ...
		BatchJob jobToBeExecuted = getBatchJob();

		boolean canRun = true;
		if (jobToBeExecuted.isEnabled()) {
			if (jobToBeExecuted.isOncePerDayJob()) {
				LocalDate today = new LocalDate();
				LOG.debug("Runing instance of {} DOES NOT EXISTS ---> CHECKING if job has already been executed today [{}]", jobName, today);
				if (hasJobBeenExecuted(jobToBeExecuted, today)) {
					LOG.debug("***** Job {} ALREADY EXECUTED today ---> SKIPPING job execution", jobName);
					canRun = false;
				}
			}
		} else {
			LOG.debug("***** Job {} DISABLED ---> SKIPPING job execution", jobName);
			canRun = false;
		}

		if (canRun) {
			// execute job ...
			LOG.debug("Job {} is valid for execution ----> STARTING", jobName);
			BatchJobExecution jobExecution = null;
			try {
				jobExecution = jobExecutionService.startNewExecution(getBatchJob(), createJobParams(p_executionContext));
				executeJob();
				jobExecution.setStatus(ExecutionStatus.SUCCESS);
			} catch (Exception ex) {
				LOG.error("Exception while executing job!", ex);
				jobExecution.setExitMessage(StringUtils.substring(ex.getMessage(), 0, 2047));
				jobExecution.setStatus(ExecutionStatus.FAIL);
			}
			jobExecution.setJobEndTime(new DateTime());
			jobExecutionService.save(jobExecution);
			LOG.debug("***** Job {} ---> FINISHED!", jobName);
		}
	}

	/**
	 * Other classes should override this method. This is default implementation
	 * which simply returns empty map.
	 * 
	 * @param p_executionContext - execution context for this Job
	 * @return  HashMap of result values.
	 */
	protected Map<String, String> createJobParams(JobExecutionContext p_executionContext) {
		return new HashMap<String, String>();
	}

	public abstract void executeJob();

	/**
	 * Default implementation. Other classes should override this method.<br>
	 * Searches for jobExecution instance of the BatchJob associated with this
	 * runner, executed today and with ExecutionStatus SUCCESS and returns true
	 * if such record is found false otherwise.<br>
	 * 
	 * @param p_jobToBeExecuted - JobToBeExecuted
	 * @param p_execDate - execution date that should be set
	 * @return true if this job has already been executed today.
	 */
	protected boolean hasJobBeenExecuted(BatchJob p_jobToBeExecuted, LocalDate p_execDate) {
		List<BatchJobExecution> executions = jobExecutionService.findJobExecutionsOnDateOfStatus(p_jobToBeExecuted, p_execDate,
				ExecutionStatus.SUCCESS, new PageRequest(0, 5));
		boolean result = false;
		if (executions.size() > 0) {
			result = true;
		}
		return result;
	}

	public BatchJob getBatchJob() {
		if (batchJob == null) {
			batchJob = jobService.findByName(jobName);
		}
		return batchJob;
	}

	// --- set / get methods --------------------------------------------------

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public BatchJobExecutionService getJobExecutionService() {
		return jobExecutionService;
	}

	public void setJobExecutionService(BatchJobExecutionService jobExecutionService) {
		this.jobExecutionService = jobExecutionService;
	}

	public BatchJobService getJobService() {
		return jobService;
	}

	public void setJobService(BatchJobService jobService) {
		this.jobService = jobService;
	}
}
