package com.ag04.batch.support.quartz.job;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ag04.batch.support.quartz.BatchJobRunner;

/**
 * 
 * @author dmadunic
 *
 */
public class JobExecutionCLeanerJob extends BatchJobRunner {
	private static final Logger LOG = LoggerFactory.getLogger(JobExecutionCLeanerJob.class);

	private int daysOld = 14;
	
	@Override
	public void executeJob() {
		LocalDate dateBefore = new LocalDate();
		dateBefore = dateBefore.minusDays(daysOld);
		LOG.info("START ---> Deleting all JobExecutions older than: dateBore='{}'", dateBefore);
		
		int numOfElementsDeleted = jobExecutionService.deleteJobExecutionsBeforeDate(dateBefore);
		LOG.info("FINISH ---> Deleted total of {} JobExecutions", numOfElementsDeleted);
	}
	
	//--- set / get methods ---------------------------------------------------

	public int getDaysOld() {
		return daysOld;
	}

	public void setDaysOld(final int p_daysOld) {
		daysOld = p_daysOld;
	}

}
