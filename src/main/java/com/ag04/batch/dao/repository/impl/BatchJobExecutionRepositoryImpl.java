package com.ag04.batch.dao.repository.impl;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import org.joda.time.LocalDate;

import com.ag04.batch.dao.repository.BatchJobExecutionRepositoryCustom;
import com.ag04.batch.model.BatchJobExecution;
import com.ag04.batch.model.QBatchJobExecution;

/**
 * 
 * @author dmadunic
 *
 */
public class BatchJobExecutionRepositoryImpl implements BatchJobExecutionRepositoryCustom {

	@PersistenceContext
    private transient EntityManager entityManager;
	
	public List<BatchJobExecution> findJobExecutionOnDateWithParamOfValue(Long jobId, LocalDate p_execDate, String p_paramName, String p_paramValue) {
		JPAQuery query = new JPAQuery(entityManager);
        final QBatchJobExecution jobExecution = QBatchJobExecution.batchJobExecution;
        
        query.from(jobExecution);

        final List<BooleanExpression> expressionList = new LinkedList<BooleanExpression>();
        
        // 1. jobId
        final BooleanExpression job = jobExecution.job.id.eq(jobId);
        expressionList.add(job);
        
        // 2. execDate
        final BooleanExpression runDate = jobExecution.runDate.eq(p_execDate);
		expressionList.add(runDate);
        
		// 3. userId param value
		final BooleanExpression userParam = jobExecution.jobParams.contains(p_paramName, p_paramValue);
        expressionList.add(userParam);
        
        BooleanExpression searchPredicate = null;
		if (!expressionList.isEmpty()) {
			searchPredicate = expressionList.remove(0);
			for (final BooleanExpression booleanExpression : expressionList) {
				searchPredicate = searchPredicate.and(booleanExpression);
			}
		}
		
		query.where(searchPredicate);
        return query.fetch();
	}

}
