package com.ag04.batch.dao.repository;

import java.util.List;

import org.joda.time.LocalDate;

import com.ag04.batch.model.BatchJobExecution;

/**
 * 
 * @author dmadunic
 *
 */
public interface BatchJobExecutionRepositoryCustom {

	List<BatchJobExecution> findJobExecutionOnDateWithParamOfValue(Long jobId, LocalDate p_execDate, String p_paramName, String p_paramValue);
}
