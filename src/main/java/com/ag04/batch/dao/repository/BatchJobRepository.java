package com.ag04.batch.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ag04.batch.model.BatchJob;
/**
 * 
 * @author dmadunic
 *
 */
public interface BatchJobRepository extends JpaRepository<BatchJob, Long> {

	BatchJob findByName(String name);
}
