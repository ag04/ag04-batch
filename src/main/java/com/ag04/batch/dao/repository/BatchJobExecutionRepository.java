package com.ag04.batch.dao.repository;

import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import com.ag04.batch.model.BatchJobExecution;
/**
 * Spring data implementation of BatchJobExecution repository.
 * 
 * @author dmadunic
 * 
 * @since 21.01.2014
 */
/**
 * Spring data implementation of BatchJobExecution repository.
 * 
 * @author dmadunic
 * 
 * @since 21.01.2014
 */
public interface BatchJobExecutionRepository extends JpaRepository<BatchJobExecution, Long>, QueryDslPredicateExecutor<BatchJobExecution>, BatchJobExecutionRepositoryCustom {

	List<BatchJobExecution> findByJobIdAndStatusId(Long p_jobId, Long p_statusId, Pageable p_pageable);
	
	List<BatchJobExecution> findByJobId(Long p_jobId, Pageable p_pageable);
	
	List<BatchJobExecution> findByJobIdAndRunDate(Long p_jobId, LocalDate p_runDate, Pageable p_pageable);
	
	List<BatchJobExecution> findByJobIdAndStatusIdAndRunDate(Long p_jobId, Long p_statusId, LocalDate p_runDate, Pageable p_pageable);
	
    @Modifying
	@Query("DELETE FROM BatchJobExecution bje WHERE bje.runDate < :runDateBefore")
	int deleteJobExecutionsBeforeDate(@Param("runDateBefore") LocalDate p_runDateBefore);
    
    @Query("SELECT bje FROM BatchJobExecution bje WHERE bje.job.id = :jobId")
    Page<BatchJobExecution> findBatchJobByJobId(@Param("jobId")Long p_jobId, Pageable p_pageabel);
}
