create or replace trigger TRG_BATCH_JOB_EXEC_INSERT
before insert on BATCH_JOB_EXEC
for each row
declare
    max_id number;
    cur_seq number;
begin
    if :new.JOB_EXEC_ID is null then
        -- No ID passed, get one from the sequence
        select BATCH_JOB_EXEC_SEQ.nextval into :new.JOB_EXEC_ID from dual;
--    else
--        -- ID was set via insert, so update the sequence
--        select greatest(nvl(max(JOB_EXEC_ID),0), :new.JOB_EXEC_ID) into max_id from BATCH_JOB_EXEC;
--        select BATCH_JOB_EXEC_SEQ.nextval into cur_seq from dual;
--        while cur_seq < max_id
--        loop
--            select BATCH_JOB_EXEC_SEQ.nextval into cur_seq from dual;
--        end loop;
    end if;
end;
/