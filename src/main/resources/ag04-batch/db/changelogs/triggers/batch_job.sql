create or replace trigger TRG_BATCH_JOB_INSERT
before insert on BATCH_JOB
for each row
declare
    max_id number;
    cur_seq number;
begin
    if :new.JOB_ID is null then
        -- No ID passed, get one from the sequence
        select BATCH_JOB_SEQ.nextval into :new.JOB_ID from dual;
--    else
--        -- ID was set via insert, so update the sequence
--        select greatest(nvl(max(JOB_ID),0), :new.JOB_ID) into max_id from BATCH_JOB;
--        select BATCH_JOB_SEQ.nextval into cur_seq from dual;
--        while cur_seq < max_id
--        loop
--            select BATCH_JOB_SEQ.nextval into cur_seq from dual;
--        end loop;
    end if;
end;
/