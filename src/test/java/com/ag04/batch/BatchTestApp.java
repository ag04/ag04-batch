package com.ag04.batch;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by dmadunic on 27/07/17.
 */
@SpringBootApplication
@EnableTransactionManagement
public class BatchTestApp {
}
