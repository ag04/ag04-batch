package com.ag04.batch.dao.repository;

import static org.fest.assertions.Assertions.assertThat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import com.ag04.support.springtestdbunit.SysdateReplacementDataSetLoader;
import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.ag04.batch.model.BatchJobExecution;
import com.ag04.batch.model.ExecutionStatus;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

/**
 * DbUnit tests for BatchJobExecutionRepository.
 * 
 * @author dmadunic
 *
 */
@DataJpaTest
@RunWith(SpringRunner.class)
@DbUnitConfiguration(dataSetLoader = SysdateReplacementDataSetLoader.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup("/META-INF/dbunit/batchJobsDataset.xml")
@Transactional
public class BatchJobExecutionRepositoryTest {

	@Autowired
	BatchJobExecutionRepository  repository;
	
	@Test
	public void testFindAll() {
		//act ...
		List<BatchJobExecution> jobExecutions = repository.findAll();
		
		//assert ...
		assertThat(jobExecutions).isNotNull();
		assertThat(jobExecutions.size()).isEqualTo(5);
	}
	
	@Test
	public void testFindOne_whenJobExecutionExists() {
		//arrange ...
		LocalDate today = new LocalDate();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String weekAgo = df.format(today.minusWeeks(1).toDate());
		
		//act ...
		BatchJobExecution jobExecution = repository.findOne(2L);
		
		//assert ...
		assertThat(jobExecution).isNotNull();
		assertThat(jobExecution.getRunDate()).isEqualTo(today);
		assertThat(jobExecution.getStatus()).isEqualTo(ExecutionStatus.SUCCESS);
		
		assertThat(jobExecution.getJob()).isNotNull();
		assertThat(jobExecution.getJob().getName()).isEqualTo("JOB_1");
		assertThat(jobExecution.getJobParams().size()).isEqualTo(2);
		
		String fromDate = jobExecution.getJobParams().get("fromDate");
		assertThat(fromDate).isEqualTo(weekAgo);
	}
	
	@Test
	public void testfindJobExecutionOnDateWithParamOfValue_whenSuchJobExecutionExists() {
		//arrange ...
		final LocalDate today = new LocalDate();
		final String userId = "111111";
				
		//act ...
		List<BatchJobExecution> jobExecutions = repository.findJobExecutionOnDateWithParamOfValue(1L, today, "userId",userId);
		
		//assert ...
		assertThat(jobExecutions).isNotNull();
		assertThat(jobExecutions.size()).isEqualTo(1);
		
		BatchJobExecution jobExecution = jobExecutions.get(0);
		
		assertThat(jobExecution.getRunDate()).isEqualTo(today);
		assertThat(jobExecution.getStatus()).isEqualTo(ExecutionStatus.SUCCESS);

		assertThat(jobExecution.getJob()).isNotNull();
		assertThat(jobExecution.getJob().getName()).isEqualTo("JOB_1");
		assertThat(jobExecution.getJobParams().size()).isEqualTo(2);

		String userIdParamValue = jobExecution.getJobParams().get("userId");
		assertThat(userIdParamValue).isEqualTo(userId);
	}
	
	@Test
	public void testfindJobExecutionOnDateWithParamOfValue_whenSuchJobExecutionDoesNotExists() {
		//arrange ...
		final LocalDate today = new LocalDate();
		final String userId = "111112";
				
		//act ...
		List<BatchJobExecution> jobExecutions = repository.findJobExecutionOnDateWithParamOfValue(1L, today, "userId",userId);
		
		//assert ...
		assertThat(jobExecutions).isNotNull();
		assertThat(jobExecutions.size()).isEqualTo(0);

	}

	@Test
	public void testDeleteJobExecution_whenJobExecutionOlderThanOneMonth() {
		//arrange ...
		final LocalDate today = new LocalDate();
		
		//act ...
		int deletedNum = repository.deleteJobExecutionsBeforeDate(today.minusWeeks(2));
		
		//assert ...
		assertThat(deletedNum).isEqualTo(2);

	}
	
	@Test
	public void testFindBatchJobByJobId(){
		//arrange
		Long jobId = 1L;
		PageRequest pageable = new PageRequest(0, 10);
		
		//act
		Page<BatchJobExecution> result = repository.findBatchJobByJobId(jobId, pageable);
		
		//assert
		assertThat(result).isNotNull();
		assertThat(result.getContent().size()).isEqualTo(3);
	}
}
