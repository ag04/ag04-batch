package com.ag04.batch.dao.repository;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import com.ag04.support.springtestdbunit.SysdateReplacementDataSetLoader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.ag04.batch.model.BatchJob;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

/**
 * DbUnit tests for BatchJobRepository.
 * 
 * @author dmadunic
 *
 */
@DataJpaTest
@RunWith(SpringRunner.class)
@DbUnitConfiguration(dataSetLoader = SysdateReplacementDataSetLoader.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("/META-INF/dbunit/batchJobsDataset.xml")
public class BatchJobRepositoryTest {

	@Autowired
	BatchJobRepository repository;
	
	@Test
	public void testFindAll() {
		//act ...
		List<BatchJob> jobs= repository.findAll();
		
		//assert ...
		assertThat(jobs).isNotNull();
		assertThat(jobs.size()).isEqualTo(3);
	}

	@Test
	public void testFindByName_whenJobExists() {
		//act ...
		BatchJob job= repository.findByName("JOB_1");
		
		//assert ...
		assertThat(job).isNotNull();
		assertThat(job.getName()).isEqualTo("JOB_1");
	}
	
	@Test
	public void testFindByName_whenJobDoesNotExists() {
		//act ...
		BatchJob job= repository.findByName("JOB_XX");
		
		//assert ...
		assertThat(job).isNull();
	}
}
