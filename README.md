ag04-batch
====================

Ag04 batch processes library.

## Usage
### Requirements
* [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Maven](https://maven.apache.org/download.cgi)

### Setup (First time)
1. Clone the repository: `git clone https://YoutUserName@bitbucket.org/ag04/ag04-batch.git`
4. Build project with: ` mvn clean install `

### Release

1) mvn release:prepare
2) mvn release:perform

## Changelog

### Version 1.0.0

Upgraded:

*

#### Dependencies:

* spring-boot :: 1.5.3.RELEASE
    * spring-boot-starter
    * spring-context-support
    * spring-web
    * spring-boot-starter-data-jpa
    * spring-boot-starter-test
    * spring-jdbc
    * spring-test
    * spring-security-core

* quartz :: 2.3.0

* querydsl-jpa :: 4.1.4

* joda-time :: 2.8.2 (resolved through spring-boot-parent)
* org.jadira.usertype : usertype.core  :: 5.0.0.GA


* junit 4.12 (resolved through spring-boot-parent)
* dbunit 2.5.3
* mockito-core :: 1.10.19 (resolved through spring-boot-parent)
* javaassist 3.20.0-GA
* fest-assert 1.4
* spring-test-dbunit 1.3.0

